#!/usr/bin/env python3

import argparse
import concurrent
import google.protobuf
import grpc
import random
import threading

import service_pb2
import service_pb2_grpc


class User:
    def __init__(self, name):
        self.name = name
        self.role = service_pb2.Role.Unknown
        self.alive = True

    def set_role(self, role):
        self.role = role

    def kill(self):
        self.alive = False

    def as_proto(self):
        return service_pb2.User(
            name=self.name,
            role=self.role,
            alive=self.alive,
        )

    def __eq__(self, other):
        return self.name == other.name


class Game:
    USERS_LIMIT = 4

    def __init__(self, id, limit=None):
        self.id = id
        self.users_limit = limit or self.USERS_LIMIT
        self.users = []

    def full(self):
        return len(self.users) == self.users_limit

    def add_user(self, user):
        self.users.append(user)

    def remove_user(self, user):
        self.users.remove(user)

    def start(self):
        print(f'Game {self.id} started')
        self.set_roles()

    def set_roles(self):
        mafiosi = len(self.users) // 3
        commissioners = len(self.users) // 3
        citizens = len(self.users) - mafiosi - commissioners
        roles = (
            [service_pb2.Role.Mafioso] * mafiosi +
            [service_pb2.Role.Commissioner] * commissioners +
            [service_pb2.Role.Citizen] * citizens
        )
        random.shuffle(roles)
        for i, role in enumerate(roles):
            self.users[i].set_role(role)


class MafiaServicer(service_pb2_grpc.MafiaServicer):
    def __init__(self):
        self.users = []
        self.games = []

    def Connect(self, request, context):
        user = User(request.name)
        if user in self.users:
            context.abort(
                code=grpc.StatusCode.INVALID_ARGUMENT,
                details=f'There is already a user with name {user.name}'
            )
        game = self.add_user(user)
        return service_pb2.ConnectResponse(game=game)

    def Disconnect(self, request, context):
        user = User(request.name)
        self.users.remove(user)
        self.games[request.game].remove_user(user)
        print(f'User {user.name} disconnected from game {request.game}')
        return service_pb2.DisconnectResponse()

    def GetGameStatus(self, request, context):
        game = self.games[request.game]
        status = service_pb2.GameStatus()

        status.game = request.game

        users = [user.as_proto() for user in game.users]
        status.users.extend(users)

        found = [user for user in users if user.name == request.name]
        status.role = found[0].role

        return status

    def add_user(self, user):
        self.users.append(user)
        if not self.games:
            self.games.append(Game(len(self.games)))
        game = self.games[-1]
        game.add_user(user)
        print(f'User {user.name} connected to game {game.id}')
        if game.full():
            thr = threading.Thread(target=game.start)
            thr.start()
            self.games.append(Game(len(self.games)))
        return game.id


class Server:
    def __init__(self, address):
        self.address = address
        self.workers = 10

    def serve(self):
        self.server = grpc.server(
            concurrent.futures.ThreadPoolExecutor(max_workers=self.workers)
        )
        service_pb2_grpc.add_MafiaServicer_to_server(MafiaServicer(), self.server)
        self.server.add_insecure_port(self.address)
        self.server.start()
        self.server.wait_for_termination()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--address', type=str, default='localhost:9000')
    return parser.parse_args()


def serve():
    args = parse_args()
    server = Server(args.address)
    server.serve()


if __name__ == '__main__':
    serve()
