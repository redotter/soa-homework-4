# RPC API

Run the server:
```bash
./server.py --address localhost:9000
```

Run the client:
```bash
./client.py --server localhost:9000 --name kenobi
```
