#!/usr/bin/env python3

import argparse
import google.protobuf
import grpc
import os
import time

import service_pb2
import service_pb2_grpc


class Client:
    def __init__(self, stub, name):
        self.stub = stub
        self.name = name
        self.game = None

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.disconnect()

    def connect(self):
        request = service_pb2.ConnectRequest(name=self.name)
        response = self.stub.Connect(request)
        self.game = response.game
        print(f'Connected to game {self.game}')

    def disconnect(self):
        request = service_pb2.DisconnectRequest(
            name=self.name,
            game=self.game,
        )
        return self.stub.Disconnect(request)

    def game_status(self):
        request = service_pb2.GameStatusRequest(
            name=self.name,
            game=self.game,
        )
        return self.stub.GetGameStatus(request)

    def print_game_status(self, status):
        os.system('clear')
        print(f'Game {status.game}')
        for user in status.users:
            alive = 'alive' if user.alive else 'dead'
            print(f'- {user.name} ({alive})')
        role = service_pb2._ROLE.values_by_number[status.role].name
        print(f'Your role: {role}')

    def loop(self):
        while True:
            status = self.game_status()
            self.print_game_status(status)
            time.sleep(1)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--name', type=str, required=True)
    parser.add_argument('--server', type=str, default='localhost:9000')
    return parser.parse_args()


def main():
    args = parse_args()
    with grpc.insecure_channel(args.server) as channel:
        stub = service_pb2_grpc.MafiaStub(channel)
        with Client(stub, args.name) as client:
            client.loop()


if __name__ == '__main__':
    main()
